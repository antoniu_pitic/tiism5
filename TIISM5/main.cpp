#include <iostream>
using namespace std;

#include "Tests.h"
#include "Questii.h"
#include "CifreNumar.h"
#include "Divizibilitate.h"

int main() {
	for (int i = 1; i <= 100; i++) {
		if (Prim(i)) {
			cout << i << " ";
		}
	}
}