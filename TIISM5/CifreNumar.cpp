#include "CifreNumar.h"
#include <iostream>
using namespace std;

int SumaCifre(int n)
{
	int S = 0;
	for (int t = n; t; t /= 10) {
		S += t % 10;
	}
	return S;
}

int ProdusCifrePare(int n)
{
	int P = 1;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c % 2 == 0) {
			P *= c;
		}
	}
	return P;
}

int CateCifreImpare(int n)
{
	int ct = 0;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c % 2 == 1) {
			ct++;
		}
	}
	return ct;
}

int MaximCifre(int n)
{
	int max = INT_MIN;
	for (int t = n; t ; t /= 10) {
		int c = t % 10;
		if (c > max) {
			max = c;
		}
	}
	return max;
}

int MinimCifreImpare(int n) // 79275
{
	int min = INT_MAX;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if ((c < min) && (c%2==1)){
			min = c;
		}
	}
	return min;
}

int Invers(int n)
{
	int m=0;
	for (int t = n; t; t /= 10) {
		m = m * 10 + t % 10;
	}
	return m;
}

bool Palindrom(int n)
{
	return n == Invers(n);
}

int PrimaCifra(int n)
{
	while (n >= 10) {
		n /= 10;
	}
	return n;
}
