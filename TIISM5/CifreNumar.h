#pragma once


int SumaCifre(int);
int ProdusCifrePare(int);
int CateCifreImpare(int);
int MaximCifre(int);
int MinimCifreImpare(int);

int Invers(int); // 1234 -> 4321
bool Palindrom(int); // simetric 1331 7881887 6
int PrimaCifra(int); // 764 -> 7