#include "Tests.h"
#include "Questii.h"
#include "CifreNumar.h"
#include "Divizibilitate.h"
#include <assert.h>

void TestAll()
{
	assert(SumaInterval(1, 10) == 55);
	assert(ProdusValoriPareDinInterval(1, 6) == 48);
	assert(CatePareInInterval(1, 10) == 5);

	assert(SumaCifre(1234) == 10);
	assert(ProdusCifrePare(1234) == 8);
	assert(CateCifreImpare(123477) == 4);
	assert(MaximCifre(12734) == 7);
	assert(MinimCifreImpare(8234) == 3);

	assert(Invers(1234) == 4321);
	assert(Palindrom(1234) == false);
	assert(Palindrom(1234321) == true);
	assert(PrimaCifra(7234) == 7);


	assert(SumaDivizori(6) == 12);
	assert(SumaDivizori(5) == 6);

	assert(Prim(17) == true);
	assert(Prim(123) == false);

	assert(Cmmdc(120, 28) == 4);
	assert(Cmmmc(15, 6) == 30);
}
