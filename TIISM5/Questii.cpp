#include "Questii.h"

int SumaInterval(int a, int b)
{
	int S = 0;
	for (int i = a; i <= b; i++) {
		S += i;
	}
	return S;
}

int ProdusValoriPareDinInterval(int a, int b)
{
	int P = 1;
	for (int i = a; i <= b; i++) {
		if (i % 2 == 0) {
			P *= i;
		}
	}
	return P;


	/*
	int P = 1;
	for (int i = a%2?a+1:a; i <= b; i+=2) {
			P *= i;
	}
	return P;
	
	*/
}

int CatePareInInterval(int a, int b)
{
	int ct = 0;
	for (int i = a; i <= b; i++) {
		if (i % 2 == 0) {
			ct++;
		}
	}
	return ct;

}
