#include "Divizibilitate.h"
#include <iostream>
using namespace std;

void AfisareDivizori(int n)
{
	for (int d = 1; d <= n; d++){
		if (n%d == 0) {
			cout << d << " "; 
		}
	}
}

int SumaDivizori(int n)
{
	int S = 0;
	for (int d = 1; d <= n; d++) {
		if (n%d == 0) {
			S += d;
		}
	}
	return S;
}

bool Prim(int n)
{
	for (int d = 2; d <= n / 2; d++) {
		if (n%d == 0) {
			return false;
		}
	}
	return true;
}

void DescompunereaInFactoriPrimi(int n)
{
	int d = 2;
	while (n > 1) {
		if (n%d == 0) {
			cout << d << " ";
			n /= d;
		}
		else {
			d++;
		}
	}
}

int Cmmdc(int a, int b)
{
	int r;

	while (b) {
		r = a%b;
		a = b;
		b = r;
	}

	return a;
}

int Cmmmc(int a, int b)
{
	return a*b / Cmmdc(a, b);
}
