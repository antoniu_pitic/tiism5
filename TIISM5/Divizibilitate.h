#pragma once

void AfisareDivizori(int);
int SumaDivizori(int);

bool Prim(int);
void DescompunereaInFactoriPrimi(int); // 120 = 2*2*2*3*5
int Cmmdc(int, int);
int Cmmmc(int, int);
